﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace WpfApp1
{

   /// <summary>
   /// Базовый класс приложения
   /// </summary>
   public partial class MainWindow : Window
    {
        /// <summary>
        /// Стартовый метод приложения. Инициализация окна приложения
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Кнопка выгрузки отформатированных данных в текстовое окно
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
            // Выгрузка файла в текстовое поле
        {
            string path = @"coordinates.txt";
            // Указываю адрес файла
            FileInfo fi = new FileInfo(path);
            // Очищаю текстовое поле перед выводом
            TextBoxF.Clear();
            // Форматирую данные из файла и вывожу в текстовое поле
            using (StreamReader sr = fi.OpenText())
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    TextBoxF.Text = TextBoxF.Text+ "\r\n" + "X: "+ s.Replace(",", " Y:");
                }
            }

        }


        /// <summary>
        /// Кнопка добавления данных, введенных пользователем, в файл с последующей выгрузкой в текстовое поле
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
            // Добавление в файл данных, введенных пользователем
        {
            string path = @"coordinates.txt";
            // Указываю адрес файла
            FileInfo fi = new FileInfo(path);
            // Добавляю данные в файл
            using (StreamWriter sw = fi.AppendText())
            {
                sw.WriteLine(TextBoxX.Text + "," + TextBoxY.Text);
            }
            // Очищаю текстовое поле перед выводом
            TextBoxF.Clear();
            // Форматирую данные из файла и вывожу в текстовое поле
            using (StreamReader sr = fi.OpenText())
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    TextBoxF.Text = TextBoxF.Text + "\r\n" + "X: " + s.Replace(",", " Y:");
                }
            }
            // Очищаю текстовые поля
            TextBoxX.Clear();
            TextBoxY.Clear();

        }
        
        /// <summary>
        /// Кнопка очищения файла. Создает новый файл, очищает текстовое окно
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_3(object sender, RoutedEventArgs e)
            //Очистка файла
        {
            string path = @"coordinates.txt";
            // Указываю адрес файла
            FileInfo fi = new FileInfo(path);
            FileStream fs = fi.Create();
            // Очищаю текстовое поле вывода файла
            TextBoxF.Clear();
            fs.Close();
        }
    }
}
