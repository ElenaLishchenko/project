﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractFactory_cake.IBiscuit;
using AbstractFactory_cake.ICream;
using AbstractFactory_cake.IBaking;

namespace AbstractFactory_cake.IBaking
{
    class MilkCakeBaking : ICBaking
    {
        public ICBiscuit BiscuitCreating()
        {
            return new Wafer_biscuit();
        }

        public ICCream CreamCreating()
        {
            return new Cream_cheese();
        }
    }
}
