﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractFactory_cake.IBiscuit;
using AbstractFactory_cake.ICream;

namespace AbstractFactory_cake
{
    interface ICBaking
    {
        ICBiscuit BiscuitCreating();
        ICCream CreamCreating();
    }
}
