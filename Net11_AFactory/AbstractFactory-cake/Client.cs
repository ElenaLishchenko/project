﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractFactory_cake.IBiscuit;
using AbstractFactory_cake.ICream;
using AbstractFactory_cake.IBaking;

namespace AbstractFactory_cake
{
    class Client
    {
        private ICBiscuit biscuit ;
        private ICCream cream;

        public Client(ICBaking baking)
        {
            this.biscuit = baking.BiscuitCreating();
            this.cream = baking.CreamCreating();
        }

    }
}
