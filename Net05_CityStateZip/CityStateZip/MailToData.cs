﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace CityStateZip
{
    /// <summary>
    /// Class for user data
    /// </summary>
    class MailToData
    {      

        //Properties of MailToData class
        public string Name { get; set;}
        public string Adress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }

        /// <summary>
        /// Method for assay user data
        /// </summary>
        /// <param name="textBoxText"></param>
        public void Assay(string textBoxText)
        {
            textBoxText = textBoxText.Trim();
            if (textBoxText=="")
            {
                MessageBox.Show("Textbox is empty");
                throw new FormatException();
            }
            if (textBoxText.Length>100)
            {
                MessageBox.Show("Too many chars");
                throw new FormatException();
            }        
        }

        /// <summary>
        /// Method try to parse user string. 
        /// </summary>
        /// <param name="citystatezip"></param>
        public void Parse(string citystatezip)
        {
            //Try to find separator "-" in user data. If not - generate FormatExeption
            int cityPosition = citystatezip.IndexOf("-");
            if (cityPosition == -1)
            {
                MessageBox.Show("Can't find a City.");
                throw new FormatException(); 
            }
            int zipPosition = citystatezip.LastIndexOf("-");
            if (zipPosition == -1)
            {
                MessageBox.Show("Can't find a Zip");
                throw new FormatException();
            }
            try
            {
                City = citystatezip.Substring(0, cityPosition);                
            }
            catch (FormatException)
            {
                MessageBox.Show("City is incorrect");
                throw;
            }
            try
            {                
                State = citystatezip.Substring(cityPosition + 1, (zipPosition - 1 - cityPosition));
            }
            catch (FormatException)
            {
                MessageBox.Show("State is incorrect ");
                throw;
            }
            int x = 0;
                if (!Int32.TryParse(citystatezip.Substring(zipPosition + 1), out x))
                    {
                    MessageBox.Show("Zip is incorrect");
                    throw new FormatException();
                }
            Zip = x;                  

    }
    }
}
