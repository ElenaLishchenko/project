﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CityStateZip
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //TBName.Focus();
        }
        /// <summary>
        /// Button enter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BEnter_Click(object sender, RoutedEventArgs e)
        {
            MailToData mailToData = new MailToData();
            
            try
            {
                //Check the validity of the data
                mailToData.Assay(TBName.Text);
                mailToData.Assay(TBAdress.Text);
                mailToData.Assay(TBCityStateZip.Text);
                //Assign user data from the textbox to fields of MailToData class
                mailToData.Adress=TBAdress.Text;
                mailToData.Name = TBName.Text; 
                //Try to parse user data from "City-State-Zip" textbox
                mailToData.Parse(TBCityStateZip.Text);
                //Display MailToData class data in the textboxes
                TBName_Copy.Text = mailToData.Name;
                TBAdress_Copy.Text = mailToData.Adress;
                TBCity_Copy.Text = mailToData.City;
                TBState_Copy.Text = mailToData.State;
                TBZip_Copy.Text = mailToData.Zip.ToString();
                TBCTZ.Text = mailToData.City + "-" + mailToData.State + "-" + mailToData.Zip;
            }
                // catch the exception
            catch (FormatException) { MessageBox.Show("Data is incorrect.Please, try again"); }           
            
        }

        private void BClear_Click(object sender, RoutedEventArgs e)
        {
            TBName_Copy.Text = "";
            TBAdress_Copy.Text = "";
            TBCity_Copy.Text = "";
            TBState_Copy.Text = "";
            TBZip_Copy.Text = "";
            TBCTZ.Text = "";
            TBName.Text = "";
            TBAdress.Text = "";
            TBCityStateZip.Text = "";

        }
    }
}
