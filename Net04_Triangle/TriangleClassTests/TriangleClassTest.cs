﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Triangle;

namespace TriangleClassTests
{
    [TestClass]
    public class TriangleClassTest
    {
        [TestMethod]
        public void Perimeter_1and1and1_3returned()
        {
            //arrange
            double a = 1;
            double b = 1;
            double c = 1;
            double expected = 3;

            //act
            TriangleClass triangle = new TriangleClass(a, b, c);
            double actual = triangle.Perimeter();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Area_1and1and1_0_433012701892219returned()
        {
            //arrange
            double a = 1;
            double b = 1;
            double c = 1;
            double expected = 0.4330;

            //act
            TriangleClass triangle = new TriangleClass(a, b, c);
            double actual = triangle.Area();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Constructor_0and1and1_ExceptionReturned()
        {
            //arrange
            double a = 1;
            double b = 1;
            double c = 0;
            
            //assert
            Assert.ThrowsException<ArgumentException>(()=>(new TriangleClass(a, b, c)));
        }

        [TestMethod]
        public void Constructor_Minus1and1and1_ExceptionReturned()
        {
            //arrange
            double a = 1;
            double b = 1;
            double c = -1;

            //assert
            Assert.ThrowsException<ArgumentException>(() => (new TriangleClass(a, b, c)));
        }

        [TestMethod]
        public void Constructor_5and1and1_ExceptionReturned()
        {
            //arrange
            double a = 1;
            double b = 1;
            double c = 5;

            //assert
            Assert.ThrowsException<ArgumentException>(() => (new TriangleClass(a, b, c)));
        }

    }
}
