﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {               
                Console.WriteLine("Press any key to try create a triangle");
                Console.ReadKey(); 
                TriangleClass triangle = new TriangleClass(1, 1, 1);              
                Console.WriteLine("Press any key to calculate the perimeter of triangle.");
                Console.ReadKey();                
                Console.WriteLine("Perimeter of triangle: {0}.", triangle.Perimeter());
                Console.WriteLine("Press any key to calculate the area of triangle.");
                Console.ReadKey();                
                Console.WriteLine("Area of triangle: {0}.", triangle.Area());
                Console.ReadKey();
            }
            catch (ArithmeticException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
