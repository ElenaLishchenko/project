﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle
{
    public class TriangleClass
    {        
        // Properties        
        double SideA { get; }
        double SideB { get; }
        double SideC { get; }

        /// <summary>
        /// Constructor. Check input data. If the data are not valid, generate exception.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public TriangleClass (double a, double b, double c)
        {
            Console.WriteLine("Star checking input lengths of the sides:{0}, {1}, {2}", a,b,c);
            if(a>0 && b>0 && c>0)
            {
                if (a + b > c && a + c > b && b + c > a)
                {
                    Console.WriteLine("A triangle will be created.");                    
                    SideA = a;
                    SideB = b;
                    SideC = c;                                 
                }
                else
                {                    
                    throw new ArgumentException("Triangle does not exist. Unacceptable values of the sides");
                }
            }
            else
            {
                throw new ArgumentException("Triangle does not exist. All sides must be positive numbers!");                
            }           
        }
                
        /// <summary>
        /// Method to calculate perimeter: P=SideA+SideB+SideC
        /// </summary>
        public double Perimeter()
        {
            try
            {
                double perimeter = Math.Round(SideA + SideB + SideC,4);
                return perimeter;            
            }
            catch (ArithmeticException ex)
            {
                Console.WriteLine("Error in calculating perimeter.");
                throw ex;
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Error in calculating perimeter. Argument is unacceptable.");
                throw ex;
            }
        }

        /// <summary>
        /// Method to calculate area using Gerone formula
        /// </summary>
        public double Area()
        {
            try
            {
                var semiperimeter = (SideA + SideB + SideC) / 2;
                double area = Math.Round(Math.Sqrt(semiperimeter * (semiperimeter - SideA) * (semiperimeter - SideB) * (semiperimeter - SideC)),4);
                return area;
            }
            catch (ArithmeticException ex)
            {
                Console.WriteLine("Error in calculating area.");
                throw ex;
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Error in calculating area. Argument is unacceptable.");
                throw ex;
            }
        }
    }
}
