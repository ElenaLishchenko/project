﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Task1
{
    class Data
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Data(string x, string y)
        {
            System.Globalization.CultureInfo ru = new System.Globalization.CultureInfo("us-US");
            System.Threading.Thread.CurrentThread.CurrentCulture = ru;

            try
            {
                X = Double.Parse(x);
                Y = Double.Parse(y);
            }
            catch (FormatException)
            {
                Console.WriteLine("Вы вели данные в неверном формате");
                throw;
            }
        }
         
    }
}
