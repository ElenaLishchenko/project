﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task1
{
    class Program
    {               
        static void Main(string[] args)
        {            
            string x;
            string y;
            // Спрашиваю пользователя, будет ли он вводить данные с клавиатуры или работать с файлом
            Console.WriteLine("Добрый день! Для ввода данных с клавиатуры нажмите 1");
            Console.WriteLine("Для вывода данных из файла нажмите 2");
            Console.WriteLine("Если ничего не нужно, нажмите 3");
            string choice = Console.ReadLine();
            switch (choice)
                // Ввод данных с клавиатуры
            {
                case "1":
                    string path = @"coordinates1.txt";                    
                    FileInfo fi1 = new FileInfo(path);
                    FileStream fs = fi1.Create();
                    fs.Close();
                    do
                    {                    
                        Console.WriteLine("Введите x(используйте . как разделитель). Например, 23.8976");
                        x = Console.ReadLine();
                        x=x.Replace(',', '.');
                        Console.WriteLine("Введите y(используйте . как разделитель). Например, 23.8976");
                        y = Console.ReadLine();
                        y = y.Replace(',', '.');
                        try
                        {
                            Data xy = new Data(x, y);
                            using (StreamWriter sw = fi1.AppendText())
                            {
                                sw.WriteLine(xy.X + "," + xy.Y);
                            }
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Попробуйте ввести данные еще раз");
                        }
                        Console.WriteLine("Вы хотите добавить новые координаты? (y-да/n-нет)");
                        choice = Console.ReadLine();
                    }
                        while (choice == "y");
                    // Вывод отформатированных данных
                    using (StreamReader sr = fi1.OpenText())
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            Console.WriteLine("X:{0}", s.Replace(",", " Y:"));
                        }
                    }
                    break;
                 case "2":
                    string path2 = @"coordinates.txt";
                    // Указываю адрес файла
                    FileInfo fi = new FileInfo(path2);
                    // Спрашиваю пользователя, нужно ли добавить данные в файл или работать с уже созданным                 
                    Console.WriteLine("Для добавления новых данных в файл нажми 1.");
                    Console.WriteLine("Для форматирования текущего файла нажми 2.");
                    string answer2 = Console.ReadLine();
                    switch (answer2)
                    {                       
                        case "1":                            
                            do
                            {
                                Console.WriteLine("Введи x");
                                x = Console.ReadLine();
                                x = x.Replace(',', '.');
                                Console.WriteLine("Введи y");
                                y = Console.ReadLine();
                                y = y.Replace(',', '.');
                                try
                                {
                                    Data xy = new Data(x, y);
                                    using (StreamWriter sw = fi.AppendText())
                                    {
                                        sw.WriteLine(xy.X + "," + xy.Y);
                                    }
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Попробуйте ввести данные еще раз");
                                }
                                Console.WriteLine("Для добавления новых координат нажми 1. ");
                                Console.WriteLine("Для форматирования существующего файла нажми 2.");
                                answer2 = Console.ReadLine();
                            }
                            while (answer2 == "y");
                            break;
                    }
                    using (StreamReader sr = fi.OpenText())
                    {
                        string s = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                            Console.WriteLine("X:{0}", s.Replace(",", " Y:"));
                        }
                    }
                    break;
            }
            Console.WriteLine("Для выхода нажмите любую клавишу");
            Console.ReadKey();
        }
    }
}
