﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Inheritance_Interface_abstract
{
    /// <summary>
    /// ProgramConverter class implements Iconvertible interface with ConvertToCSharp and ConvertToVB methods. Contains two public string fields - Code and Language.
    /// </summary>
    class ProgramConverter : Iconvertible
    {
        public string Code { get; set; }
        public string Language { get; set; }

        public ProgramConverter(string code, string language)
        {
            Code = code;
            Language = language;
        }
        public string ConvertToCSharp(string code)
        {
            return "Convert your code to CSharp: Console.Writeline('Hello world')";
        }

        public string ConvertToVB(string code)
        {
            return "Convert your code to VB: Console.Writeline('Hello world')";
        }
    }
}
