﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Inheritance_Interface_abstract
{
    /// <summary>
    /// class "Program". Main program class.
    /// </summary>
    class Program
    {

        /// <summary>
        /// Starting method.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            ProgramConverterArray array = new ProgramConverterArray();
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    array.TestArray(i);
                }
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Error! {0}",e);
            }
            Console.ReadKey();
        }
    }
}
