﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Inheritance_Interface_abstract
{
    /// <summary>
    /// IConvertible interface defines ConvertToCSharp and ConvertToVB methods
    /// </summary>
    interface Iconvertible
    {
        string ConvertToCSharp(string code);
        string ConvertToVB(string code);
    }
    
}
