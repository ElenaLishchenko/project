﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Inheritance_Interface_abstract
{
    /// <summary>
    /// ICodeChecker Interface defines ICodeChecker method
    /// </summary>
    interface ICodeChecker
    {
        bool CheckCodeSyntax(string code, string language);
    }
}
