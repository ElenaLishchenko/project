﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Inheritance_Interface_abstract
{
    /// <summary>
    /// ProgramConvertToArray class. 
    /// </summary>
    class ProgramConverterArray
    {
        private ProgramConverter[] array = new ProgramConverter[10];

        /// <summary>
        /// ProgramConvertToArray constructor. Assigns values to array elements. 5 instances of ProgramConverter class and 5 instances of ProgramHelper class
        /// </summary>
        public ProgramConverterArray()
        {
            array[0] = new ProgramConverter("This is my code on VB", "VB");
            array[1] = new ProgramConverter("This is my code on VB", "VB");
            array[2] = new ProgramConverter("This is my code on CSharp", "CSharp");
            array[3] = new ProgramConverter("This is my code on CSharp", "CSharp");
            array[4] = new ProgramConverter("This is my code on CSharp", "CSharp");
            array[5] = new ProgramHelper("This is my code on VB", "VB");
            array[6] = new ProgramHelper("This is my code on VB", "VB");
            array[7] = new ProgramHelper("This is my code on CSharp", "CSharp");
            array[8] = new ProgramHelper("This is my code on CSharp", "CSharp");
            array[9] = new ProgramHelper("This is my code on CSharp", "CSharp");
        }

        // Indexer declaration. Get acsessor only
        public ProgramConverter this[int index]
        {
            get
            {
                return array[index];
            }
        }

        /// <summary>
        /// Testarray method. Test element x of array.
        /// </summary>
        /// <param name="x"></param>
        public void TestArray(int x)
        {
            var asprogramHelper = array[x] as ProgramHelper;
            var asprogramConverter = array[x] as ProgramConverter;
            if (asprogramHelper == null && asprogramConverter != null)
            {
                Console.WriteLine("Class ProgramConverter");
                Console.WriteLine(asprogramConverter.ConvertToCSharp(array[x].Code));
                Console.WriteLine(asprogramConverter.ConvertToVB(array[x].Code));
            }
            else if (asprogramHelper != null)
            {
                Console.WriteLine("Class ProgramHelper");
                try
                {
                    if (asprogramHelper.CheckCodeSyntax(asprogramHelper.Code, asprogramHelper.Language))
                    {
                        if (asprogramHelper.Language == "CSharp")
                        {
                            Console.WriteLine(asprogramConverter.ConvertToCSharp(array[x].Code));
                        }
                        else 
                        {
                            Console.WriteLine(asprogramConverter.ConvertToVB(array[x].Code));
                        }                       
                    }
                    else
                    {
                        throw new ArgumentException("Incorrect code");
                    }
                }
                catch (ArgumentException)
                {
                    throw ;
                }
            }
            else
            {
                throw new ArgumentException("Incorrect element of array:"+ x);
            }
        }
    }
}
