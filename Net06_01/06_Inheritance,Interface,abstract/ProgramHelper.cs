﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Inheritance_Interface_abstract
{
    /// <summary>
    /// ProgramHelper class inherits from ProgramConverter class and implements ICodeCheker interface with CheckCodeSyntax method. 
    /// </summary>
    class ProgramHelper : ProgramConverter, ICodeChecker
    {
        public ProgramHelper(string code, string language) : base(code, language)
        {
        }
        /// <summary>
        /// CheckCodeSyntax method. Checks the code and language. 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public bool CheckCodeSyntax(string code, string language)
        {            
                if (String.IsNullOrWhiteSpace(code) || String.IsNullOrWhiteSpace(language) )
                {
                    throw new ArgumentException("Code and language can not be null or empty");
                }
                if (language == "CSharp" || language=="VB")
                {
                    if (code.Length > 10)
                    {
                        return true;
                    }   
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new ArgumentException("Language must be CSharp or VB");
                }            
        }
        
    }
}
