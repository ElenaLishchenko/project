﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;



namespace _09_TextRedactor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {    
       /// <summary>
       /// Property
       /// </summary>
        public static FileRedactorClass Frc { get; set; }        
        public MainWindow()
        {
            InitializeComponent();            
            Frc = new FileRedactorClass(new TextRange(RTBText.Document.ContentStart, RTBText.Document.ContentEnd));
        }

        /// <summary>
        /// Button "Open" to open text file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Frc.Open();
            }            
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Ошибка: {0}", ex.Message);
            }            
        }

        /// <summary>
        /// Button "Save" to save file without Save File Dialog. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Frc.Save(new TextRange(RTBText.Document.ContentStart, RTBText.Document.ContentEnd));
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Ошибка: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Button "Save As" to open Save Dialog. User can choose the file name.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BSaveAs_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Frc.SaveAs(new TextRange(RTBText.Document.ContentStart, RTBText.Document.ContentEnd));
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Ошибка: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Button "Close" to close application. Create Meaasage Box with yes/no buttons.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BClose_Click(object sender, RoutedEventArgs e)
        {
            {
                MessageBoxResult result = MessageBox.Show("Действительно хотите выйти? Все несохраненные изменения будут потеряны", "Выход из приложения", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    Application.Current.Shutdown();
                }
            }

        }
    }
}