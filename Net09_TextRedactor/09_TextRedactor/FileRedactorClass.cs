﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace _09_TextRedactor
{
    /// <summary>
    /// File Redactor Class - work with text
    /// </summary>
    public class FileRedactorClass
    { 
        //Properties
        private static string UserFileName {get; set;}
        private static TextRange Doc { get; set; }

        public FileRedactorClass (TextRange doc)
        {
            Doc = doc;
        }
    
        /// <summary>
        /// Open file method
        /// </summary>
        /// <param name="doc"></param>
        public void Open()
        {
            try
            {            
                OpenFileDialog openFD = new OpenFileDialog();
                openFD.Filter = "Text files|*.rtf; *.txt";
                if (openFD.ShowDialog() == true)
            {
                using (FileStream fs = new FileStream(openFD.FileName, FileMode.Open))
                {

                    if (Path.GetExtension(openFD.FileName).ToLower() == ".rtf")
                    {
                        Doc.Load(fs, DataFormats.Rtf);
                    }
                    else if (System.IO.Path.GetExtension(openFD.FileName).ToLower() == ".txt")
                    {
                        Doc.Load(fs, DataFormats.Text);
                    }
                }
            }
            }
            catch (UnauthorizedAccessException ex) { throw new FileNotFoundException("Невозможно получить доступ к файлу",ex);}   
            catch (OutOfMemoryException ex) { throw new FileNotFoundException("Недостаточно памяти для открытия файла", ex); }
            catch (InvalidOperationException ex) { throw new FileNotFoundException("Ошибка при попытке доступа к файлу", ex); }
            catch (IOException ex) { throw new FileNotFoundException("Ошибка при открытии файла", ex); }
            catch (SystemException ex) { throw new FileNotFoundException("Системная ошибка при открытии файла", ex); }
            catch (Exception ex) { throw new FileNotFoundException("Ошибка в приложении", ex); }
            }

        /// <summary>
        /// Save file method
        /// </summary>
        public void Save(TextRange doc)
        {
            Doc = doc;            
            try
            {
            if (UserFileName == null || Doc == null )
            {
                MessageBox.Show("Документ не открыт");
                return;
            }
            if (Doc.Text.Length < 3)
            {
                MessageBoxResult result = MessageBox.Show("Документ пустой. Все равно сохранить?", "Сохранение документа", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }
            using (FileStream fs = File.Create(UserFileName))
            {
                if (Path.GetExtension(UserFileName).ToLower() == ".rtf")
                {
                    Doc.Save(fs, DataFormats.Rtf);
                }
                else if (Path.GetExtension(UserFileName).ToLower() == ".txt")
                {
                    Doc.Save(fs, DataFormats.Text);
                }
            }
            }
            catch (UnauthorizedAccessException ex) { throw new FileNotFoundException("Невозможно получить доступ к файлу", ex); }
            catch (OutOfMemoryException ex) { throw new FileNotFoundException("Недостаточно памяти для сохранения файла", ex); }
            catch (InvalidOperationException ex) { throw new FileNotFoundException("Ошибка при попытке доступа к файлу", ex); }
            catch (IOException ex) { throw new FileNotFoundException("Ошибка при сохранении файла", ex); }
            catch (SystemException ex) { throw new FileNotFoundException("Системная ошибка при сохранении файла", ex); }
            catch (Exception ex) { throw new FileNotFoundException("Ошибка в приложении", ex); }

        }

        /// <summary>
        /// Save file with another name
        /// </summary>
        public void SaveAs(TextRange doc)
        {
            Doc = doc;            
            try
            {                
               if (Doc.Text.Length < 3)
                {
                    MessageBoxResult result = MessageBox.Show("Документ пустой. Все равно сохранить?", "Сохранение документа", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.No)
                    {
                        return;
                    }
                }
                SaveFileDialog SaveFD = new SaveFileDialog();
                SaveFD.Filter = "Text Files|*.txt;*.rtf";
                if (SaveFD.ShowDialog() == true)
                {
                    UserFileName = SaveFD.FileName;
                    using (FileStream fs = File.Create(UserFileName))
                    {
                        if (Path.GetExtension(UserFileName).ToLower() == ".rtf")
                        {
                            Doc.Save(fs, DataFormats.Rtf);
                        }
                        else if (Path.GetExtension(UserFileName).ToLower() == ".txt")
                        {
                            Doc.Save(fs, DataFormats.Text);
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException ex) { throw new FileNotFoundException("Невозможно получить доступ к файлу", ex); }
            catch (OutOfMemoryException ex) { throw new FileNotFoundException("Недостаточно памяти для сохранения файла", ex); }
            catch (InvalidOperationException ex) { throw new FileNotFoundException("Ошибка при попытке доступа к файлу", ex); }
            catch (IOException ex) { throw new FileNotFoundException("Ошибка при сохранении файла", ex); }
            catch (SystemException ex) { throw new FileNotFoundException("Системная ошибка при сохранении файла", ex); }
            catch (Exception ex) { throw new FileNotFoundException("Ошибка в приложении", ex); }
        }        

    }
}
