﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    /// <summary>
    /// Class Newton for calculate root of numeric using Newton method.
    /// </summary>
    class Newton
    {
        // Class Newton properties
        public double Numeric { get; set; }
        double Approximation { get; set; }
        public double Exponent { get; set; }

        // Class Newton constructor. Try convert user dates to Double format. Catch exceptions.
        public Newton(string numeric, string approximation, string exponent)
        {
            System.Globalization.CultureInfo ru = new System.Globalization.CultureInfo("ru-RU");
            System.Threading.Thread.CurrentThread.CurrentCulture = ru;
            int x;
            if (!int.TryParse(exponent, out x))
            {
                Console.WriteLine("Exponent must be integer");
                throw new FormatException();
            }
            try
            {
                Numeric = Convert.ToDouble(numeric);
                Approximation = Convert.ToDouble(approximation);
                Exponent = Convert.ToDouble(exponent);
            }
            catch (FormatException)
            {
                Console.WriteLine("Your data is incorrect");
                throw;
            }
            if (Exponent<=0)
            {
                Console.WriteLine("Exponent must be >=0");
                throw new FormatException();
            }
            if (Approximation < 0)
            {
                Console.WriteLine("Approximation must be >0");
                throw new FormatException();
            }
            if (Numeric <= 0)
            {
                Console.WriteLine("Numeric must be >0");
                throw new FormatException();
            }
        }
        // Exponentiation method. Raises x to (n-1) exponent.
        public double Exp(double x1, double exponent)
        {
            double result = 1;
            for (int i = 1; i < exponent; i++)
                {
                    result = result * x1;
                }                
            
            return result;
        }
        // Calculation method. Returns the n-exponent root of numeric.
        public double Calc()
        {
            try
            {
                double x1 = Numeric / Exponent;
                double x1Exp = Exp(x1, Exponent);
                double x2 = (1 / Exponent) * ((Exponent - 1) * x1 + Numeric / x1Exp);
                double app = Math.Abs(x2 - x1);
                while (app > Approximation)
                {
                    x1 = x2;
                    x1Exp = Exp(x1, Exponent);
                    x2 = (1 / Exponent) * ((Exponent - 1) * x1 + Numeric / x1Exp);
                    app = Math.Abs(x2 - x1);
                }
                return x2;
            }
            catch (FormatException)
            {
                Console.WriteLine("Calculation error");
                throw;
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Calculation error. Can't divide by zero.");
                throw;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                Console.WriteLine("Error of numeric range");
                throw;
            }
        }

       
    }
    /// <summary>
    /// class "Program". Main program class.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Starting method.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("Enter numeric (positive integer)");
            string numeric = Console.ReadLine();
            Console.WriteLine("Enter exponent (positive integer)");
            string exponent = Console.ReadLine();
            Console.WriteLine("Enter approximation (ex.0,0001)");
            string approximation = Console.ReadLine();

            try
            {
                Newton calculator = new Newton(numeric, approximation, exponent);
                Console.WriteLine("The root:" + calculator.Calc());
                Console.WriteLine("Using Math.Pow:" + Math.Pow(calculator.Numeric, (1 / calculator.Exponent)));
            }
             catch (FormatException)
             { Console.WriteLine("Error.Press any key.");
                 Console.ReadKey();
             }
             catch (DivideByZeroException)
             {
                 Console.WriteLine("Error.Press any key.");
                 Console.ReadKey();
             }
             catch (ArgumentOutOfRangeException)
             {
                 Console.WriteLine("Error.Press any key.");
                Console.ReadKey();
             }
            Console.ReadKey();
        }
    }
}
