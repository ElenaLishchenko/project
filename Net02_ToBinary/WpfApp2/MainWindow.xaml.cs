﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Class Tobinary. Convert integer to binary string
    /// </summary>
    // Class Tobinary property
    class Tobinary
    {
        int Numeric { get; set; }

        //Class Tobinary constructor.Try convert user date to Integer format.Catch format exception.
        public Tobinary(string numeric)
        {
            try
            {
                Numeric = Convert.ToInt32(numeric);
            }
            catch (FormatException)
            {
                MessageBox.Show("Your numeric is incorrect. Insert positive integer numeric.");
                throw;
            }
            if (Numeric<0)
            {
                MessageBox.Show("Error. Numeric<0");
                throw new FormatException();
            }
        }
        //Class Tobinary method. Convert user date to binary form. Return the string.
        public string ToBinary()
        {
            try
            {
                string result = Convert.ToString(Numeric, 2);
                return result;
            }
            catch (FormatException)
            {
                MessageBox.Show("Your numeric is incorrect. Insert positive integer numeric.");
                throw;
            }
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //Method for Convert button. Create an instance of Tobinary class. Call his method to convert user date. Show the result to ConvertToBinary textbox.
        private void ConvertButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Tobinary convert = new Tobinary(UserNumeric.Text);
                ConvertToBinary.Text = convert.ToBinary();
            }
            catch (FormatException)
            {
                MessageBox.Show("Error. Try again");
                UserNumeric.Text = "";
                ConvertToBinary.Text = "";
            }
        }
    }
}
