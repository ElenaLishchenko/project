﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task2_3.Decorator
{
    /// <summary>
    /// Main profram class
    /// </summary>
    class Program
    {               
        static void Main(string[] args)
        {
            //Task 2
            Console.WriteLine("Task 2");
            string pathSource = @"text.txt";
                double count = File.ReadAllLines(pathSource).Length;
                string line = "";
                double i = 1;
                Stream f = new FileStream(pathSource, FileMode.Open, FileAccess.Read);
                StreamReader reader = new ReadFileDecorator(f, count);          
            try
            {
                while ((line = reader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                    i++;                    
                }
                reader.Close();
                Console.WriteLine("Press any key to continue");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}",ex.Message);
                Console.ReadLine();
            }
            finally
            {
                reader.Close();
            }

            // Task 3
            Console.WriteLine("Task 3");            
            i = 0;
            Stream f2 = new FileStream(pathSource, FileMode.Open, FileAccess.Read);
            StreamReader reader2 = new PasswordDecorator(f2);
            try
            {
                while ((line = reader2.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                    i++;
                }
                reader2.Close();
                Console.ReadLine();
            }
            catch (FieldAccessException ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
                Console.ReadLine();
            }
            finally
            {
                reader2.Close();
            }
        }
    }
}
