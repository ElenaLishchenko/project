﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task2_3.Decorator
{
    /// <summary>
    /// ReadFileDecorator class. Decorate Stream. Add scroll view
    /// </summary>
    class ReadFileDecorator : StreamReader
    {
        public double Count { set; get; }
        public double I;
        public ReadFileDecorator(Stream stream, double count) : base(stream)
        {
            Count = count;
            I = 0;
            Console.WriteLine("Press any key to reading. File size: {0} string",count);
        }

        public override string ReadLine()
        {
            if (I>0)
            {
                Console.WriteLine(Convert.ToInt32(I / (Count / 100))+"%");
            }
            I += 1;            
            Console.ReadKey();
            return base.ReadLine();
        }
    }
}
