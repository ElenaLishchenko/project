﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task2_3.Decorator
{
    /// <summary>
    /// PasswordDecorator class. Decorate Stream. Add password validation
    /// </summary>
    class PasswordDecorator : StreamReader
    {
        public PasswordDecorator(Stream stream) : base(stream)
        {
        }

        public override string ReadLine()
        {
            Console.WriteLine("Enter your password (password)");
            if(Console.ReadLine()!="password")
            {
                throw new FieldAccessException("Wrong password");
            }
            return base.ReadLine();
        }
    }
}
