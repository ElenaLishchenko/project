﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace CWork2
{
    class Program
    {
        // Задание 1
        public static IEnumerable<string> Collection(IEnumerable<string> stringList, string findString)
        {
            var query = from s in stringList
                        where s.Contains(findString)
                        select s;
            return query;
        }

        
        public static IEnumerable<string> DirList(string dirName)
        {
            yield return dirName;
            string[] dirs = Directory.GetDirectories(dirName);
            foreach (string dir in dirs)
                yield return dir;
        }

        public static IEnumerable<string> FileList(IEnumerable<string> dirList)
        {
            foreach (string dir in dirList)
            {
                string[] files = Directory.GetFiles(dir);
                foreach (string file in files)
                  yield return file;
            }
        }

        //Метод, написанный к заданию 2, но в третьем оказался не нужен
        public static IEnumerable<string> findFileList(IEnumerable<string> fileName, string findString)
        {
            foreach (string s in fileName)
            {
                using (StreamReader sr = new StreamReader(s))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains(findString))
                        {
                            yield return s;
                            break;
                        }
                    }
                }
            }
        }

        public static IEnumerable<string> stringFileList(string fileName)
        {            
                using (StreamReader sr = new StreamReader(fileName))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {                       
                            yield return line;                           
                    }
                }            
        }

        //Задание 3
        public static IEnumerable<Report> ReportList(IEnumerable<string> fileList, string findString)
        {
            foreach (string file in fileList)
                {
                int i = 0;
                using (StreamReader sr = new StreamReader(file))
                {                                     
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains(findString))
                            i++;
                    }

                }
                if (i != 0)
                {
                    yield return new Report
                    {
                        FindString = findString,
                        Count = i,
                        FileName = Path.GetFileNameWithoutExtension(file),
                        FilePath = file,
                        ChangeDate = File.GetLastWriteTime(file).ToString(),
                        FileSize = (new FileInfo(file)).Length.ToString(),
                    };
                    i = 0;
                    }
            }
        }

     

        static void Main(string[] args)
        {
            try
            {
                string dirName = "";
                string findString = "test";

                if (!Directory.Exists(dirName))
                    dirName = (Directory.GetCurrentDirectory());

                // Задание 2
                foreach (string name in findFileList(FileList(DirList(dirName)), findString))
                    Console.WriteLine(name);

                var query = from s in ReportList(FileList(DirList(dirName)), findString)
                            orderby s.Count
                            select s;
                foreach (Report data in query)
                {
                    Console.WriteLine(data.FileName);
                    Console.WriteLine(data.FilePath);
                    Console.WriteLine(data.FileSize);
                    Console.WriteLine(data.FindString);
                    Console.WriteLine(data.Count);
                }

                XmlSerializer serializer = new XmlSerializer(typeof(Report));
                // Сериализация
                foreach (Report rep in query)
                {
                    using (var Stream = new FileStream("Serialization.xml", FileMode.Create, FileAccess.Write, FileShare.Read))
                    {
                        serializer.Serialize(Stream, rep);
                    }
                        }


                    Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

        }
    }
}
