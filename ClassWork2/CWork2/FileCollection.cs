﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWork2
{
    class FileCollection
    {     

        public static IEnumerable<string> Collection(string dirName, string findString)
        {
            string[] dirs = Directory.GetDirectories(dirName);
            foreach (string dir in dirs)
            {
                string[] files = Directory.GetFiles(dir);
                foreach (string s in files)
                {
                    using (StreamReader sr = new StreamReader(s))
                    {
                        string line;
                        // Read and display lines from the file until the end of 
                        // the file is reached.
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.Contains(findString))
                            {
                                yield return s;
                                yield break;
                            }
                        }
                    }

                }

            }
        }


    }
}
