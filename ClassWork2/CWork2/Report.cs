﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWork2
{
    class Report
    {
        public string FindString { get; set; }
        public int Count { get; set; }
        
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string ChangeDate { get; set; }
        public string FileSize { get; set; }       

    }

}
