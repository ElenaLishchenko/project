﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Task10Timer
{
    // Делегат, используемый для поддержки события
    delegate void Init();
    /// <summary>
    /// Класс, создающий таймер обратного отсчета
    /// </summary>
    class TimerClass
    {
        // Событие, возникающее при отсчете времени
        public event Init InitEvent;
        // Событие, возникающие по окончании работы таймера
        public static event Action<string, int> Finished;
        public int TimerTime { get; set; }
        public string TimerName { get; set; }          
        public int TimeLeft { get; set; }
        public void Tick()
        {
            for (TimeLeft = TimerTime; TimeLeft != 0; TimeLeft--)
            {    
                if (InitEvent!=null)
                    InitEvent();
                Thread.Sleep(1000);
            }
            if (Finished != null)
                Finished(TimerName, TimerTime);
        }

        public TimerClass (string timername, int timertime)
        {            
            TimerTime = timertime;
            TimerName = timername;            
        }

        public void TimerStart (TimerClass timer, Init initMethod, Action<string,int> finished)
        {
            InitEvent += initMethod;
            Finished = finished;
            Thread Thread1 = new Thread(Tick);            
            Thread1.Start();
                     
        }   
    }
}
