﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10Timer
{
    class HandlingByMethod:ICutDownNotifier
    {
        public Action<string,int> TaskStart;        
        public Action<string, int> TaskFinish;
        public TimerClass MyTimer { get; set; }      
        
        public HandlingByMethod (Action<string, int> taskStart, Action<string, int> taskFinish)
        {
            TaskStart = taskStart;
            TaskFinish = taskFinish;
        }        

        public void Init()
        {
            Console.WriteLine("Имя таймера:{0}. Осталось {1} секунд",MyTimer.TimerName, MyTimer.TimeLeft);
        }

        public void Run(string timername, int timertime)
        {
                MyTimer = new TimerClass(timername, timertime);
            MyTimer.TimerStart(MyTimer,Init, TaskFinish);
           
        }
    }
}
