﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task10Timer
{
    /// <summary>
    /// Класс Program
    /// </summary>
    class Program
    {
        // Событие типа делегата Action
        public static event Action<string, int> Started;        

        // Статические поля - наименование задания и время работы таймера
        static string TaskName = "10.Delegate and event";
        static int TimerTime = 5;

        /// <summary>
        /// Методы, разработанные для передачи в качестве параметров конструкторов пользовательских классов.
        /// </summary>
        /// <param name="timerName"></param>
        /// <param name="time"></param>
        static void TimerStart(string timerName, int time)
        {
            Console.WriteLine("Запущен таймер задачи:{0}, который будет работать {1} секунд", timerName, time);
        }
        static void TimerFinish(string timerName, int time)
        {
            Console.WriteLine("Закончил работу таймер:{0}, который проработал {1} секунд", timerName, time);
        }

        /// <summary>
        /// Метод Main. Начало работы приложения
        /// </summary>
        static void Main()
        {
            // Создание делегатов для передачи в конструкторах
            Action<string, int> FirstTimerStart = TimerStart;
            Action<string, int> FirstTimerFinish = TimerFinish;
            try
            {
                /// Создание массива для хранения экземпляров объектов пользовательских классов
                ICutDownNotifier[] myArray = new ICutDownNotifier[3];
                /// Передача в качестве обработчиков делегатов методов
                HandlingByMethod hbm = new HandlingByMethod(FirstTimerStart, FirstTimerFinish);                
                Started += hbm.TaskStart;
                myArray[0] = hbm;

                // Передача анонимных делегатов
                HandlingByAnonDelegate hbad = new HandlingByAnonDelegate(delegate (string timerName, int time)
                {
                    Console.WriteLine("Запущен таймер задачи:{0}, который будет работать {1} секунд", timerName, time);
                }, delegate (string timerName, int time)
                {
                    Console.WriteLine("Закончил работу таймер: {0}, который проработал {1} секунд", timerName, time);
                });
                Started += hbad.TaskStart;
                myArray[1] = hbad;

                // Передача лямбда-выражений
                HandlingByLambda hbl = new HandlingByLambda((timerName, time) => { Console.WriteLine("Запущен таймер задачи {0}, который будет работать {1} секунд", timerName, time); },
                                                       (timerName, time) => { Console.WriteLine("Закончил работу таймер:{0}, который проработал {1} секунд", timerName, time); });
                Started += hbl.TaskStart;
                myArray[2] = hbl;

                //Запуск события
                if (Started != null)
                    Started(TaskName, TimerTime);
                // Запуск таймеров
                myArray[0].Run("Чтение задания", TimerTime);
                myArray[1].Run("Выполнение задания", TimerTime);
                myArray[2].Run("Проверка задания перед отправкой", TimerTime);
            }
                catch (System.ArgumentException ex)
            {
                Console.WriteLine("Ошибка! Некорректное значение аргумента:{0}", ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Произошла системная ошибка:{0}",ex.Message);
            }
            

        Console.ReadKey();         
        }
        
    }
}

