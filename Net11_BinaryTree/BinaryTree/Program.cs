﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    /// <summary>
    /// Program class
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main method of program
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Create array of integer and add it to the tree
            int[] array = { 1, 3, 5, 7, 9, 2, 4, 6, 8, 10 };
            BinaryTreeClass<int> tree1 = new BinaryTreeClass<int>(array, new IntegerComparer());
            foreach (var item in tree1)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
            
            // Create list of string and add it ti the tree
            List<string> stringCollection = new List<string>
            {
                "string1",
                "string2",
                "string3",
                "string4",
                "string5"
            };
            BinaryTreeClass<string> tree2 = new BinaryTreeClass<string>(stringCollection);
            foreach (var item in tree2)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();

            // Create list of StudentClass instances and add it to the tree
            List<StudentClass> studentCollection = new List<StudentClass>();
            studentCollection.Add(new StudentClass("Elena", "test", "13.07", 5));
            studentCollection.Add(new StudentClass("Andrey", "test", "13.07", 3));
            studentCollection.Add(new StudentClass("Evgeniy", "test", "13.07", 1));
            studentCollection.Add(new StudentClass("Dmitry", "test", "13.07", 4));
            studentCollection.Add(new StudentClass("Vladimir", "test", "13.07", 2));
            BinaryTreeClass<StudentClass> tree3 = new BinaryTreeClass<StudentClass>(studentCollection, new StudentsComparer());
            foreach (var item in tree3)
            {
                Console.WriteLine("Name:{0} Test:{1} Date:{2} Mark:{3}",item.Name,item.TestName,item.TestDate,item.Mark);
            }
            Console.ReadKey();


        }
    }
}
