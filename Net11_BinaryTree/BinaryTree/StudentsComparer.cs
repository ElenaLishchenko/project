﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    /// <summary>
    /// Comparer interface for StudentClass
    /// </summary>
    class StudentsComparer: Comparer<StudentClass>
    {  
        
        public override int Compare(StudentClass x, StudentClass y)
        {
            if (object.Equals(x, y)) return 0; 
            return x.Mark.CompareTo(y.Mark);
        }
    }
}