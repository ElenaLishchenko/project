﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    /// <summary>
    /// Comparer interface for integer 
    /// </summary>
    public class IntegerComparer :Comparer<int>

    {
        public override int Compare(int x, int y)
            {
                if (object.Equals(x, y)) return 0;
                return x.CompareTo(y);
            }
        
    }
}
