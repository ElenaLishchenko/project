﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{ 
    /// <summary>
    /// TreeElementsClass. Contains elements of binary tree
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TreeElementClass<T> : IComparable<T> where T : IComparable
    {
        BinaryTreeClass<T> _tree;

        TreeElementClass<T> _left;   
        TreeElementClass<T> _right; 

        public TreeElementClass( T value, TreeElementClass<T> parent,BinaryTreeClass<T> tree)
        {
            _tree = tree;
            Parent = parent;
            Value = value;
        }
        public TreeElementClass<T> Left
        {
            get
            {
                return _left;
            }

            internal set
            {
                _left = value;

                if (_left != null)
                {
                    _left.Parent = this;  
                }
            }
        }

        public TreeElementClass<T> Right
        {
            get
            {
                return _right;
            }

            internal set
            {
                _right = value;

                if (_right != null)
                {
                    _right.Parent = this; 
                }
            }
        }        

        public TreeElementClass<T> Parent
        {
            get;
            internal set;
        }

        // значение текущего узла 

        public T Value
        {
            get;
            private set;
        }

        // Возвращет 1, если значение экземпляра больше переданного значения,  
        // возвращает -1, когда значение экземпляра меньше переданого значения, 0 - когда они равны.     

        public int CompareTo(T other)
        {
            return Value.CompareTo(other);
        }

    }
}
