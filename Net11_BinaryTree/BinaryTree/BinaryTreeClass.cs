﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    /// <summary>
    /// BinaryTreeClass with type parameter T 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BinaryTreeClass<T> : IEnumerable<T> where T : IComparable

    {
        public TreeElementClass<T> TreeHead { get; set; }
        public int Count { get; set; }


        public BinaryTreeClass(T[] array,Comparer<T> comparer)
        {            
            Array.Sort(array, comparer);
            int i;
            for (i=0; i<array.Length; i++)
            {
                this.Add(array[i]);
            }            
        }

        public BinaryTreeClass(IEnumerable<T> collection)
        {
            foreach (T element in collection)
            {
                this.Add(element);

            }
        }

        public BinaryTreeClass(List<T> collection, Comparer<T> comparer)
        {
            collection.Sort(comparer);
            foreach (T element in collection)
                {
                  this.Add(element);
                }
        }

        /// <summary>
        /// Method for creating new binary tree
        /// </summary>
        /// <param name="value"></param>
        public void Add(T value)
        {
            // If the tree is clear  
            if (TreeHead == null)
            {
                TreeHead = new TreeElementClass<T>(value, null, this);
            }
            
            else
            {
                AddTo(TreeHead, value);
            }

            Count++;
        }
        /// <summary>
        /// Method for creating new element of the tree
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public void AddTo(TreeElementClass<T> element, T value)
        {                 
            if (value.CompareTo(element.Value) < 0)
            {          
                if (element.Left == null)
                {
                    element.Left = new TreeElementClass<T>(value, element, this);
                }
                else
                {
                    
                    AddTo(element.Left, value);
                }
            }            
            else
            {              
                if (element.Right == null)
                {
                    element.Right = new TreeElementClass<T>(value, element, this);
                }
                else
                {                           
                    AddTo(element.Right, value);
                }
            }            
        }    
        /// <summary>
        /// Iterator
        /// </summary>
        /// <returns></returns>
         
           public IEnumerator<T> InOrderTraversal()
           {            

               if (TreeHead != null) 
               {
                   Stack<TreeElementClass<T>> stack = new Stack<TreeElementClass<T>>();
                   TreeElementClass<T> current = TreeHead;              
                   bool goLeftNext = true;                   
                   stack.Push(current);
                  while (stack.Count > 0)
                   {                      
                       if (goLeftNext)
                       {                       

                           while (current.Left != null)
                           {
                               stack.Push(current);
                               current = current.Left;
                           }
                       }

                       yield return current.Value;                    

                       if (current.Right != null)
                       {
                           current = current.Right;
                           goLeftNext = true;
                       }
                       else
                       {                         
                           current = stack.Pop();
                           goLeftNext = false;
                       }
                   }
               }
           }

           public IEnumerator<T> GetEnumerator()
           {
               return InReverseOrder();
           }

           System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
           {

               return GetEnumerator();

           }
        /// <summary>
        /// Iterator in reverse order
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> InReverseOrder()
        {

            if (TreeHead != null)
            {
                Stack<TreeElementClass<T>> stack = new Stack<TreeElementClass<T>>();
                TreeElementClass<T> current = TreeHead;
                bool goRightNext = true;
                stack.Push(current);
                while (stack.Count > 0)
                {
                    if (goRightNext)
                    {

                        while (current.Right != null)
                        {
                            stack.Push(current);
                            current = current.Right;
                        }
                    }

                    yield return current.Value;

                    if (current.Left != null)
                    {
                        current = current.Left;
                        goRightNext = true;
                    }
                    else
                    {
                        current = stack.Pop();
                        goRightNext = false;
                    }
                }
            }
        }


    }
}
