﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
    
{
    /// <summary>
    /// StudentClass for information about students.
    /// </summary>
    class StudentClass : IEnumerable, IComparable
    {        
        public string Name { get; private set; }
        public string TestName {  get; private set; }
        public string TestDate {  get; private set; }
        public int Mark { get; private set; } 
        
        public StudentClass(string name, string testName, string testDate, int mark)
        {
            Name = name;
            TestName = testName;
            TestDate = testDate;
            Mark = mark;
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public int CompareTo(object obj)
        {            
                StudentClass otherStudent = obj as StudentClass;
                int x;
                if (Mark > otherStudent.Mark) x = 1;
                else
                {
                    if (Mark < otherStudent.Mark) x = -1;
                    else x = 0;
                }
                return x;
            
        }
    }
}
