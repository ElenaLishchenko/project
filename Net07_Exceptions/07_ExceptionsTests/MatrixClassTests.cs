﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using _07_Exceptions;

namespace _07_ExceptionsTests
{
    [TestClass]
    public class MatrixClassTests
    {
        [TestMethod]
        public void MatrixMultiply_11_11and222_222return444_444()
        {
            //arrange
            MatrixClass testmatrix1 = new MatrixClass(2,2);
            for (int i = 0; i < 2; i++)
            {
                for (int n = 0; n < 2; n++)
                {
                    testmatrix1.matrix[i][n] = 1;
                }
            }
            
            MatrixClass testmatrix2 = new MatrixClass(3, 2);
            for (int i = 0; i < 2; i++)
            {
                for (int n = 0; n < 3; n++)
                {
                     testmatrix2.matrix[i][n] = 2;
                }
            }

            MatrixClass expectedmatrix = new MatrixClass(3, 2);
            for (int i = 0; i < 2; i++)
            {
                for (int n = 0; n < 3; n++)
                {
                    expectedmatrix.matrix[i][n] = 4;
                }
            }            

            //act            
            MatrixClass testmultipliedmatrix = testmatrix1.MatrixMultiply(testmatrix2);

            //assert
            for (int i = 0; i < 2; i++)
            {
                for (int k = 0; k < 3; k++)
                {
                    Assert.AreEqual(expectedmatrix.matrix[i][k], testmultipliedmatrix.matrix[i][k]);
                }
            }
            
        }

        [TestMethod]
        public void MatrixMultiply_11_11and111_111_111returnException()
        {
            //arrange
            MatrixClass testmatrix1 = new MatrixClass(2, 2);
            for (int i = 0; i < 2; i++)
            {
                for (int n = 0; n < 2; n++)
                {
                    testmatrix1.matrix[i][n] = 1;
                }
            }

            MatrixClass testmatrix2 = new MatrixClass(3, 3);
            for (int i = 0; i < 3; i++)
            {
                for (int n = 0; n < 3; n++)
                {
                    testmatrix2.matrix[i][n] = 1;
                }
            }

            //act               
              //  MatrixClass testmultipliedmatrix = testmatrix1.MatrixMultiply(testmatrix2);

            //assert      
            Assert.ThrowsException<ArgumentException>(() => testmatrix1.MatrixMultiply(testmatrix2));            
        }
    }
}
