﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _07_Exceptions
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// MatrixToText method unloads matrix to textbox.
        /// </summary>
        /// <param name="mmatrix"></param>
        /// <param name="textbox"></param>
        private void MatrixToText (MatrixClass mmatrix, TextBox textbox)
        {
            textbox.Text = "";
            for (int i1 = 0; i1 < mmatrix.matrix.Length; ++i1)
            {
                for (int i2 = 0; i2 < mmatrix.matrix[0].Length; ++i2)
                {
                    textbox.Text += mmatrix.matrix[i1][i2] + " ";
                }
                textbox.Text += Environment.NewLine;
            }
        }        
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Method for Enter button. Create two random matrix, multiplies them and unloads to textboxes. Catch exceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void TBEnter_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            MatrixClass matrix1 = new MatrixClass(Convert.ToInt32(TBx1.Text), Convert.ToInt32(TBy1.Text));
            matrix1.RandomGenerate();
            MatrixToText(matrix1, TBMatrix1);
            MatrixClass matrix2 = new MatrixClass(Convert.ToInt32(TBx2.Text), Convert.ToInt32(TBy2.Text));            
            matrix2.RandomGenerate();            
            MatrixToText(matrix2, TBMatrix2);            
            MatrixClass multipliedmatrix = matrix1.MatrixMultiply(matrix2);
            MatrixToText(multipliedmatrix, TBMultiplies);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Incorrect dates. X and Y must be integer dates. "+ex.Message);
            }
            catch (ArithmeticException ex)
            {
                MessageBox.Show("Incorrect dates." + ex.Message);
            }

        }
    }
}
