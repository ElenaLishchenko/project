﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_Exceptions
{
    /// <summary>
    /// Class Matrix Class for create and multiply a matrix
    /// </summary>
    public class MatrixClass
    {
        public int[][] matrix;

        /// <summary>
        /// MatrixClass constructor. Create a matrix with user params: x rows, y columns. If user entered invalid length, generate exception.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public MatrixClass(int x, int y)
        {
            if (x==0 || y==0 || x>15 || y>15 )
            {
                throw new ArgumentException("Invalid matrix length. Can't create a matrix.");
            }
            matrix = new int[Math.Abs(y)][];
            for (int i = 0; i < Math.Abs(y); i++)
            {
                matrix[i] = new int[Math.Abs(x)];
            }            
        }

        /// <summary>
        /// RandomGenerate method generate random values for each matrix elements
        /// </summary>
        public void RandomGenerate()
        {
            Random rand = new Random();
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int n = 0; n < matrix[0].Length; n++)
                {
                    matrix[i][n] = rand.Next(1, 50);
                }
            }
        }

        /// <summary>
        /// MatrixMultiply method multiplies two matrices and return multiplied matrix. If the matrices are incompatible, generate exception
        /// </summary>
        /// <param name="matrix2"></param>
        /// <returns></returns>
        public MatrixClass MatrixMultiply(MatrixClass matrix2)
        {
            int l = matrix[0].Length;
            int m = matrix.Length;
            int n = matrix2.matrix[0].Length;
            if (l != matrix2.matrix.Length)
            {
                throw new ArgumentException("Invalid matrix length. Can't multiply.");
            }
            MatrixClass multipliedMatrix = new MatrixClass(n, m);
            for (int i = 0; i < m; i++)
            {
                for (int k = 0; k < n; k++)
                {
                    for (int p = 0; p < l; p++)
                    {
                        multipliedMatrix.matrix[i][k] = multipliedMatrix.matrix[i][k] + matrix[i][p] * matrix2.matrix[p][k];
                    }
                }
            }
            return multipliedMatrix;
        }        
    }
}
